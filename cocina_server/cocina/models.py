# encoding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.core.validators import MaxValueValidator

# Create your models here.
@python_2_unicode_compatible
class Ingredient(models.Model):
    name = models.TextField(null=False)
    energy_kj = models.SmallIntegerField(null=True)
    energy_kcal = models.SmallIntegerField(null=True)
    water = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    protein = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    total_fat = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    total_carbohydrate = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    
    def __str__(self):
        return self.name
    
@python_2_unicode_compatible
class RecipeIngredient(models.Model):
    amount = models.CharField(max_length=10)
    is_mandatory = models.BooleanField(default=True)
    ingredient = models.OneToOneField(Ingredient)
    
    def __str__(self):
        return self.amount + " | " + self.ingredient.name
    
@python_2_unicode_compatible
class Recipe(models.Model):
    LEVELS = (
              (0, 'Muy simple'),
              (1, 'Simple'),
              (2, 'Dificultad media'),
              (3, 'Difícil'),
              (4, 'Muy difícil'),
              )
    FOOD_TYPE = (
                 (0, 'Vegano'),
                 (1, 'Vegetariano'),
                 (2, 'Carnivoro'),
                 (3, 'Omnívoro'),
                 (4, 'Extraño')
                 )
    rating = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)])
    difficulty_level = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)], choices=LEVELS)
    cook_time = models.TimeField()
    short_description = models.CharField(max_length=250)
    photo = models.ImageField(upload_to="images")
    preparation = models.TextField()
    ingredient = models.ManyToManyField(RecipeIngredient)
    amount_person = models.IntegerField(default=1)

    def __str__(self):
        return self.short_description
