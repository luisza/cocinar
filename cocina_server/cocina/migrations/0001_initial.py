# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ingredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField()),
                ('energy_kj', models.SmallIntegerField(null=True)),
                ('energy_kcal', models.SmallIntegerField(null=True)),
                ('water', models.DecimalField(null=True, max_digits=5, decimal_places=2)),
                ('protein', models.DecimalField(null=True, max_digits=5, decimal_places=2)),
                ('total_fat', models.DecimalField(null=True, max_digits=5, decimal_places=2)),
                ('total_carbohydrate', models.DecimalField(null=True, max_digits=5, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Recipe',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(5)])),
                ('difficulty_level', models.PositiveSmallIntegerField(choices=[(0, 'Muy simple'), (1, 'Simple'), (2, 'Dificultad media'), (3, 'Dif\xedcil'), (4, 'Muy dif\xedcil')], validators=[django.core.validators.MaxValueValidator(5)])),
                ('cook_time', models.TimeField()),
                ('short_description', models.CharField(max_length=250)),
                ('photo', models.ImageField(upload_to='images')),
                ('preparation', models.TextField()),
                ('amount_person', models.IntegerField(default=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RecipeIngredient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.CharField(max_length=10)),
                ('is_mandatory', models.BooleanField(default=True)),
                ('ingredient', models.OneToOneField(to='cocina.Ingredient')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='recipe',
            name='ingredient',
            field=models.ManyToManyField(to='cocina.RecipeIngredient'),
            preserve_default=True,
        ),
    ]
