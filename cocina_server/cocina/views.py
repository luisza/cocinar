from django.shortcuts import render

import json
from django.http import HttpResponse
from .models import Recipe, Ingredient

from django.core import serializers

# Create your views here.

def top10(request):
    recipes = Recipe.objects.all().order_by('rating')[:10]
    response_data = {}
    for rep in recipes:
        response_data[rep.pk] = {
                                 'rid': str(rep.pk),
                                 'description': rep.short_description,
                                 'food_image': rep.photo.url,
                                 'difficulty': rep.get_difficulty_level_display(),
                                 'cook_time': rep.cook_time.isoformat()  # .strftime("%M/%S")
                                 }
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def get_recipe(request, pk):
    recipes = Recipe.objects.filter(pk=pk)
    if len(recipes):
        recipe = recipes[0]
        response_data = {
                    'calorie' : '0',
                    'difficulty_level': recipe.get_difficulty_level_display(),
                    'cook_time': recipe.cook_time.isoformat(),
                    'photo': recipe.photo.url,
                    'id': recipe.pk,
                    'preparation': recipe.preparation,
                    'ingredients': []
                }
        for ing in recipe.ingredient.all():
            response_data['ingredients'].append({
                            'amount': ing.amount,
                            'is_mandatory': ing.is_mandatory,
                            'name' : ing.ingredient.name,
                            'energy_kj' : ing.ingredient.energy_kj,
                            'energy_kcal' : ing.ingredient.energy_kcal,
                            'water' : str(ing.ingredient.water),
                            'protein' : str(ing.ingredient.protein),
                            'total_fat' : str(ing.ingredient.total_fat),
                            'total_carbohydrate' : str(ing.ingredient.total_carbohydrate),
                            'id': ing.ingredient.pk
                            })
    else:
        response_data = {
                         'error': 'Not found'
                         }
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def get_favorite(request):
    pks = request.POST.get('pks').split(",")
    recipes = Recipe.objects.filter(pk__in=pks)
    response_data = {}
    for rep in recipes:
        response_data[rep.pk] = {
                                 'rid': str(rep.pk),
                                 'description': rep.short_description,
                                 'food_image': rep.photo.url,
                                 'difficulty': rep.get_difficulty_level_display(),
                                 'cook_time': rep.cook_time.isoformat()  # .strftime("%M/%S")
                                 }
    return HttpResponse(json.dumps(response_data), content_type="application/json")    

def filter_ingredient(request):
    text = request.POST.get('text')
    response_data = list(Ingredient.objects.filter(name__contains=text).values('pk', 'name')[:10])
    return HttpResponse(json.dumps(response_data), content_type="application/json")   

def get_ingredients(request):
    pks = request.POST.get('pks').split(",")
    response_data = list(Ingredient.objects.filter(pk__in=pks).values('pk', 'name'))
    return HttpResponse(json.dumps(response_data), content_type="application/json")   
