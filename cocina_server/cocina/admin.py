from django.contrib import admin

from ajax_select.admin import AjaxSelectAdmin
from ajax_select import make_ajax_form

# Register your models here.

from .models import Ingredient, RecipeIngredient, Recipe

class RecipeAdmin(admin.ModelAdmin):
    list_display = ('rating', 'difficulty_level', 'cook_time', 'short_description')

class RecipeIngredientAdmin(AjaxSelectAdmin):
    form = make_ajax_form(RecipeIngredient, {'ingredient': 'ingredient'})
    
admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Ingredient)
admin.site.register(RecipeIngredient, RecipeIngredientAdmin)
