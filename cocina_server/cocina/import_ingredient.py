from .models import Ingredient 

from decimal import Decimal

ingredients = []
with open("/home/usuario/Desktop/cocina/cocina_server/cocina/data/ingredientes.csv") as arch:
    
    for line in arch:
        name, energy_kj, energy_kcal, water, protein, total_fat, total_carbohydrate = line.split('\t')
        
        energy_kj = energy_kj if not energy_kj in ('', '...') else None
        energy_kcal = energy_kcal if not energy_kcal in ('', '...') else None
        
        
        water = water.replace(',', '.')
        protein = protein.replace(',', '.')
        total_fat = total_fat.replace(',', '.')        
        total_carbohydrate = total_carbohydrate.replace(',', '.').replace('\n', '')
                        
        water = Decimal(water) if not water in ('', '...') else None        
        protein = Decimal(protein) if not protein in ('', '...') else None
        total_fat = Decimal(total_fat) if not total_fat in ('', '...') else None        
        total_carbohydrate = Decimal(total_carbohydrate) if not total_carbohydrate in ('', '...') else None        
        
                
        ingredients.append(Ingredient(
                                name=name,
                                energy_kj=energy_kj,
                                energy_kcal=energy_kcal,
                                water=water,
                                protein=protein,
                                total_fat=total_fat,
                                total_carbohydrate=total_carbohydrate
                                )
                           )
    Ingredient.objects.bulk_create(ingredients)
