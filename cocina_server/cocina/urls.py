'''
Created on Jan 15, 2015

@author: usuario
'''
from django.conf.urls import patterns, url, include

from .views import top10, get_recipe, get_ingredients
from .views import get_favorite, filter_ingredient

urlpatterns = patterns('',
                       url('^top10$', top10),
                       url('^recipe/(\d)$', get_recipe),
                       url('^favorite$', get_favorite),
                       url('^filter_ingredient$', filter_ingredient),
                       url('^ingredients$', get_ingredients))
