def _st(text):
    
    if type(text) == str:
        try:
            text = text.decode('utf-8')
        except:
            try:
                text = text.decode('iso-8859-1')
            except:
                text = unicode(text, errors='ignore')

    return text
