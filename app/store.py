'''
Created on Feb 3, 2015

@author: usuario
'''


from kivy.storage.jsonstore import JsonStore
db = JsonStore('db.json')

def add_remove_ingredient(pk):
    global db
    ingredients = []
    if db.exists('myingredient'):
        ingredients = db.get('myingredient')['ids']
    else:
        db.put('myingredient', ids=[])

    if pk in ingredients:
        ingredients.remove(pk)
        dev = 'img/false.png'
    else:
        ingredients.append(pk)
        dev = 'img/true.png'
    db.put('myingredient', ids=ingredients)
    return dev

def has_ingredient(pk):
    global db
    ingredients = []
    dev = 'img/false.png'
    if db.exists('myingredient'):
        ingredients = db.get('myingredient')['ids']
        if pk in ingredients:
            dev = 'img/true.png'
        else:
            dev = 'img/false.png'
    return dev    

def add_remove_recipe(pk):
    global db
    recipes = []
    if db.exists('favorite'):
        recipes = db.get('favorite')['ids']
        
    if pk in recipes:
        recipes.remove(pk)
        dev = 'img/recipe/false.png'
    else:
        recipes.append(pk)
        dev = 'img/recipe/true.png'
    db.put('favorite', ids=recipes) 
    return dev 
    
def recipe_in_favorite(pk):
    global db
    recipes = []
    dev = 'img/recipe/false.png'
    if db.exists('favorite'):
        recipes = db.get('favorite')['ids']
        if pk in recipes:
            dev = 'img/recipe/true.png'   
    return dev

def get_favorite():
    global db
    recipes = []
    if db.exists('favorite'):
        recipes = db.get('favorite')['ids']  
    return recipes

def get_ingredients():
    global db
    if db.exists('myingredient'):
        return db.get('myingredient')['ids']
    return []    
