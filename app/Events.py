# encoding: utf-8
from __future__ import unicode_literals

from views.Cook_ListView import RecipeList
from .Cook_Globals import HOST_URL

import urllib

from kivy.network.urlrequest import UrlRequest
from app.store import get_favorite
from views.Search_View import Search_Ingredient



def search(body_widget):
	body_widget.clear_widgets()
	search = Search_Ingredient()
	body_widget.add_widget(search)

def i_want(body_widget):
	body_widget.clear_widgets()
	recipes = RecipeList()
	recipes.body_widget = body_widget 
	favorites = get_favorite()
	if favorites:
		params = urllib.urlencode({'pks': ",".join(map(str, favorites))})
		headers = {'Content-type': 'application/x-www-form-urlencoded',
	          'Accept': 'text/plain'}
		req = UrlRequest(HOST_URL + '/favorite', on_success=recipes.update_recipes, req_body=params, req_headers=headers)
	body_widget.add_widget(recipes)


def top_10(body_widget):
	recipes = RecipeList()
	recipes.body_widget = body_widget 
	req = UrlRequest(
    HOST_URL + '/top10',
    recipes.update_recipes)
	body_widget.clear_widgets()
	body_widget.add_widget(recipes)


