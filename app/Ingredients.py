'''
Created on Feb 10, 2015

@author: usuario
'''
import urllib

from .Cook_Globals import HOST_URL
from kivy.network.urlrequest import UrlRequest


def filter_ingredient(body_widget, text):
    params = urllib.urlencode({'text': text })
    headers = {'Content-type': 'application/x-www-form-urlencoded',
          'Accept': 'text/plain'}
    req = UrlRequest(HOST_URL + '/filter_ingredient', on_success=body_widget.update_ingredients, req_body=params, req_headers=headers)


def get_ingredients(body_widget, pks):
    pks = ",".join(map(str, pks))
    params = urllib.urlencode({'pks': pks })
    headers = {'Content-type': 'application/x-www-form-urlencoded',
          'Accept': 'text/plain'}
    req = UrlRequest(HOST_URL + '/ingredients', on_success=body_widget.update_ingredients, req_body=params, req_headers=headers)


