from kivy.app import App
from kivy.uix.screenmanager import ScreenManager, Screen
from views.Start_View import Start_View
from kivy.uix.tabbedpanel import TabbedPanel

from views import *
from app.Events import i_want, top_10

__version__ = '0.1'


class Main(TabbedPanel):
	
	def __init__(self, **kwargs):
		super(Main, self).__init__(**kwargs)
		# self.bind(current_tab=self.contentChanged_cb)
		# self.bind(default_tab=self.my_default_tab_callback)



	def switch_to(self, header):
		'''Switch to a specific panel header.
		'''
		if header.text == 'Deseo':
			i_want(header.content)		
		elif header.text == 'Top 10':
			top_10(header.content)
		elif header.text == 'Inicio':
			header.content.clear_widgets()
			view = Start_View.Start_View()
			view.body_widget = header.content
			header.content.add_widget(view)
		super(Main, self).switch_to(header)



class CookApp(App):
	
	def build(self):
		return Main()
	

if __name__ == '__main__':
	CookApp().run()
