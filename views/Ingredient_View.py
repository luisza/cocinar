'''
Created on Feb 2, 2015

@author: usuario
'''

from kivy.uix.listview import ListItemButton  # At top of file
from kivy.uix.boxlayout import BoxLayout
from kivy.adapters.listadapter import ListAdapter
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.listview import ListView
from kivy.lang import Builder
from app.store import add_remove_ingredient, has_ingredient

Builder.load_file('views/ingredientItem.kv')
class IngredientListItem(BoxLayout, ListItemButton):
    rid = StringProperty()
    name = ObjectProperty()
    state = ObjectProperty()
    amount = StringProperty()
    is_mandatory = ObjectProperty()
    
    def __init__(self, *args, **kwargs):
        super(IngredientListItem, self).__init__(*args, **kwargs)
        self.ids.state.background_normal = has_ingredient(self.rid)
            
    def select_ingredient(self):
        self.ids.state.background_normal = add_remove_ingredient(self.rid)


class IngredientList(BoxLayout):
    list_view = ObjectProperty()

    def __init__(self, *args, **kwargs):
        super(IngredientList, self).__init__(*args, **kwargs)
        
        # data = [RecipeListItem('1', 'esto no se que es', 'img/receta.jpg'),]
        list_adapter = ListAdapter(data=[],
                                   args_converter=self.ingredient_converter,
                                   cls=IngredientListItem,
                                   selection_mode='none',
                                   allow_empty_selection=False)
        
        self.list_view = ListView(adapter=list_adapter)
        
        self.list_view.adapter.bind(on_selection_change=self.select_ingredient)
        
        self.add_widget(self.list_view)

        
    def ingredient_converter(self, index, obj):
        return {'rid': obj[0],
                'name': obj[1],
                'amount': obj[2],
                'is_mandatory': obj[3],
                'state': obj[4],
     
                }

    def set_ingredients(self, ingredients):
        self.list_view.adapter.data = ingredients

    def select_ingredient(self, *args):
        pass

