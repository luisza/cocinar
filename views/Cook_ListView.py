from kivy.uix.listview import ListItemButton  # At top of file
from kivy.uix.boxlayout import BoxLayout

from kivy.uix.listview import ListView
from kivy.adapters.listadapter import ListAdapter
from kivy.properties import ObjectProperty, StringProperty
from kivy.lang import Builder
from app.Cook_Globals import HOST_URL, HOST_URL_MEDIA
from views.Recipe_View import watch_recipe

Builder.load_file('views/recipeList.kv')
class RecipeListItem(BoxLayout, ListItemButton):
	rid = StringProperty()
	description = ObjectProperty()
	food_image = ObjectProperty()
	difficulty = ObjectProperty()
	cook_time = ObjectProperty()
	
	

class RecipeList(BoxLayout):
	list_view = ObjectProperty()
	body_widget = ObjectProperty()

	first_recive = False
	def __init__(self, *args, **kwargs):
		super(RecipeList, self).__init__(*args, **kwargs)
		
		# data = [RecipeListItem('1', 'esto no se que es', 'img/receta.jpg'),]
		list_adapter = ListAdapter(data=[],
		                           args_converter=self.recipe_converter,
		                           cls=RecipeListItem,
		                           selection_mode='single',
		                           allow_empty_selection=False)
		
		self.list_view = ListView(adapter=list_adapter)
		

		
		self.add_widget(self.list_view)
		self.list_view.adapter.bind(on_selection_change=self.select_recipe)
		
	def recipe_converter(self, index, obj):
		return {'rid': obj[0],
				'description': obj[1],
				'food_image': obj[2],
				'difficulty': obj[3],
				'cook_time': obj[4]}

	def update_recipes(self, request, data):
		list_data = []
		for key, recipe in data.items():
			list_data.append((
				recipe['rid'],
				recipe['description'],
				HOST_URL_MEDIA + recipe['food_image'],
				recipe['difficulty'],
				recipe['cook_time']		
							))
		self.first_recive = True
		self.list_view.adapter.data = list_data

		
	def select_recipe(self, *args):
		adapter = args[0]
		if not self.first_recive:
			watch_recipe(self.body_widget, adapter.selection[0].rid)
		else:
			self.first_recive = False

