# encoding: utf-8
from __future__ import unicode_literals

from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty, StringProperty
from app.Cook_Globals import HOST_URL, HOST_URL_MEDIA
from kivy.network.urlrequest import UrlRequest
from views.Ingredient_View import IngredientList
from app.store import add_remove_recipe, recipe_in_favorite

Builder.load_file('views/recipe.kv')
class Recipe_View(BoxLayout):
	body_widget = None
	favorite = ObjectProperty(None)
	recipe_id = None
	
	def in_favorite(self):
		self.ids.favorite.background_normal = recipe_in_favorite(self.recipe_id)
		
	def set_favorite(self):
		self.ids.favorite.background_normal = add_remove_recipe(self.recipe_id)

	def set_recipe(self, request, data):
		ing = []
		for ingr in data['ingredients']:
			ing.append((str(ingr['id']), ingr['name'], str(ingr['amount']), "img/" + str(ingr['is_mandatory']).lower() + '.png', "active"))
		self.recipe_id = data['id'] 
		self.ids.r_image.source = HOST_URL_MEDIA + data['photo']
		self.ids.information.text = "**Tiempo:**  " + str(data['cook_time']) + " **Dificultad:** " + str(data['difficulty_level']) + " **Calorías** 0!"
		
		ingredients = IngredientList()
		ingredients.set_ingredients(ing)
		self.ids.ingredients.add_widget(ingredients)
		self.ids.preparation.text = "Preparación\n---------------\n" + data['preparation']
		self.in_favorite()
		
	def set_body_widget(self, body_widget):
		self.body_widget = body_widget
		
def watch_recipe(body_widget, id_recipe):
	body_widget.clear_widgets()
	recipe_view = Recipe_View()
	req = UrlRequest(
				    HOST_URL + '/recipe/' + id_recipe,
				    recipe_view.set_recipe)
	body_widget.add_widget(recipe_view)
