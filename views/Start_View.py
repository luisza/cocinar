# encoding: utf-8
from __future__ import unicode_literals

from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout

Builder.load_file('views/start.kv')
class Start_View(BoxLayout):
    body_widget = None

