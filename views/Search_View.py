# encoding: utf-8
from __future__ import unicode_literals

from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.uix.textinput import TextInput
from app import _st
from kivy.uix.dropdown import DropDown
from app.Ingredients import filter_ingredient, get_ingredients
from kivy.uix.button import Button
from app.store import add_remove_ingredient
from app.store import get_ingredients as store_get_ingredients
# from app.Events import filter_ingredient


class SearchInput(TextInput):
    dropdown = None
    auto_complete = ObjectProperty()
    
    def insert_text(self, substring, from_undo=False):  
        filter_ingredient(self, _st(self.text + substring))         
        return super(SearchInput, self).insert_text(substring, from_undo=from_undo)
        
    def update_ingredients(self, request, data):
        
        if self.dropdown is None:
            self.dropdown = DropDown()
        self.dropdown.clear_widgets()
        for dat in data:
            btn = Button(text=dat['name'], size_hint_y=None, height=34)
            setattr(btn, 'pk', dat['pk'])
            btn.bind(on_release=lambda btn: self.parent.parent.select(btn))
            self.dropdown.add_widget(btn)
        self.dropdown.open(self)
        

        
Builder.load_file('views/search.kv')
class Search_Ingredient(BoxLayout):
    body_widget = None
    search_input = ObjectProperty()
    slider_time = ObjectProperty()
    slider_difficulty = ObjectProperty()
    auto_complete = ObjectProperty()
    list_ingredients = ObjectProperty()
    
    def __init__(self, **kwargs):
        super(Search_Ingredient, self).__init__(**kwargs)
        ingredients = store_get_ingredients()
        get_ingredients(self, ingredients)
        
#         
#     def on_text_change(self, instance, value):
#         return instance, value
        
    def on_time_change(self, value):
        H = {
             35: "1 hora",
             40: "1 hora y 15 min",
             45: "1 hora y 30 min",
             50: "1 hora y 45 min",
             55: "2 horas",
             60: "2 horas y 30 min",
             65: "3 horas",
             70: "4 horas o más"
             }
        value = int(value)
        if value <= 30:
            return "%d min" % value
        else:
            return H[value]
        
    
    def on_difficulty_change(self, value):
        value = int(value)
        LEVELS = {
              0: 'Muy simple',
              1: 'Simple',
              2: 'Dificultad media',
              3: _st('Difícil'),
              4: _st('Muy difícil'),
              }
        if value in LEVELS:
            return LEVELS[value]
        return value

    def select(self, btn):
        self.list_ingredients.item_strings.append(btn.text)
        self.search_input.text = ""
        add_remove_ingredient(btn.pk)
        self.search_input.dropdown.dismiss()
        
    def update_ingredients(self, request, data):
        # self.list_ingredients.item_strings.clear()
        for dat in data:
            self.list_ingredients.item_strings.append(dat['name'])
